#!/usr/bin/env python3
from stable_baselines import PPO2
from example_pushing_training_env import ExamplePushingTrainingEnv
from example_pushing_training_env import FlatObservationWrapper
from rrc_simulation.gym_wrapper.envs import cube_env

import argparse
import os
import gym
#!/usr/bin/env python3
from stable_baselines.common.vec_env import SubprocVecEnv
from stable_baselines import HER, DQN, SAC, TD3, PPO2 # DDPG,
from stable_baselines.her import GoalSelectionStrategy, HERGoalEnvWrapper
from stable_baselines.common.bit_flipping_env import BitFlippingEnv
from stable_baselines.common.policies import MlpPolicy
from stable_baselines.common import set_global_seeds
from stable_baselines.common.callbacks import CheckpointCallback

from example_pushing_training_env import ExamplePushingTrainingEnv
from example_pushing_training_env import FlatObservationWrapper

import argparse
import os
import gym
import numpy as np
import numpy as np


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    #parser.add_argument("--model_path", required=True, help="model path")
    #parser.add_argument("--time_steps", required=True, help="time steps")

    #args = vars(parser.parse_args())
    #time_steps = int(args["time_steps"])
    #model_path = str(args["model_path"])
    
    
     # WARNING: you must pass an env
    # or wrap your environment with HERGoalEnvWrapper to use the predict method
     
    model_class = DQN  # works also with SAC, DDPG and TD3
    N_BITS = 10
    env = BitFlippingEnv(N_BITS, continuous=model_class in [SAC, TD3], max_steps=N_BITS) # DDPG,
    model = HER.load('./her_bit_env', env=env)
    
  

    #obs = env.reset()
    #for _ in range(100):
    #    action, _ = model.predict(obs)
    #    obs, reward, done, _ = env.step(action)
#
 #       if done:
  #          obs = env.reset()


   # policy_path = os.path.join(
    #    model_path, "model_" + str(time_steps) + "_steps"
    #)

    #model = PPO2.load(policy_path)

    # define a method for the policy fn of your trained model
    def policy_fn(obs):
        return model.predict(obs, deterministic=True)[0]

    # we create the same env as we used for training in train_pushing_ppo.py,
    # such that action and observation space remain coherent with the policy.
    # however, unlike during the training, we set the initialization to the the
    # same as in the standard CubeEnv, since this is what the policy will be
    # evaluated on eventually.
    initializer = cube_env.RandomInitializer(difficulty=1) # difficulty one means pushing
    env = ExamplePushingTrainingEnv(initializer=initializer, 
                                    frameskip=3, 
                                    visualization=True)
    env = FlatObservationWrapper(env)
     
    for _ in range(10):
        obs = env.reset()
        done = False
        while not done:
            obs, rew, done, info = env.step(policy_fn(obs))

        