def get_initial_state(self):
        """Get a random initial object pose (always on the ground)."""
        return move_cube.sample_goal(difficulty=-1)

def get_goal(self):
    """Get a random goal depending on the difficulty."""
    self.difficulty = np.random.choice(self.difficulties)
    return move_cube.sample_goal(difficulty=self.difficulty)
