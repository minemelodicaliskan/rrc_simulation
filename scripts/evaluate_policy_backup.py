#!/usr/bin/env python3
"""Example evaluation script to evaluate a policy.

This is an example evaluation script for evaluating a "RandomPolicy".  Use this
as a base for your own script to evaluate your policy.  All you need to do is
to replace the `RandomPolicy` and potentially the Gym environment with your own
ones (see the TODOs in the code below).

This script will be executed in an automated procedure.  For this to work, make
sure you do not change the overall structure of the script!

This script expects the following arguments in the given order:
 - Difficulty level (needed for reward computation)
 - initial pose of the cube (as JSON string)
 - goal pose of the cube (as JSON string)
 - file to which the action log is written

It is then expected to initialize the environment with the given initial pose
and execute exactly one episode with the policy that is to be evaluated.

When finished, the action log, which is created by the TriFingerPlatform class,
is written to the specified file.  This log file is crucial as it is used to
evaluate the actual performance of the policy.
"""
import sys
import gym
from rrc_simulation.gym_wrapper.envs import cube_env
from rrc_simulation.tasks import move_cube
from stable_baselines import A2C
from scripts.training_env import TrainingEnv, FlatObservationWrapper

class RandomPolicy:
    """Dummy policy which uses random actions."""

    def __init__(self, action_space):
        self.action_space = action_space

    def predict(self, observation):
        return self.action_space.sample()

class A2CPolicy:

    def __init__(self, path):
        self.a2c_policy = A2C.load(path)

    def predict(self, observation):
        return self.a2c_policy.predict(observation, deterministic=True)[0]

def main():
    try:
        difficulty = int(sys.argv[1])
        # for difficulty level 2:
        # initial_pose_json = sys.argv[2]
        initial_pose_json = move_cube.sample_goal(difficulty=difficulty)
        # for difficulty level 2: (0, 0, 0.0825)
        # goal_pose_json = sys.argv[3]
        goal_pose_json = move_cube.sample_goal(difficulty=difficulty)
        # output_file = sys.argv[4]
        if difficulty == 1:
            output_file = 'push_output'
        elif difficulty == 2:
            output_file = 'lift_output'
        elif difficulty == 3:
            output_file = 'lift_move_output'
        else:
            output_file = 'lift_move_rotate_output'
    except IndexError:
        print("Incorrect number of arguments.")
        print(
            "Usage:\n"
            "\tevaluate_policy.py <difficulty_level> <initial_pose>"
            " <goal_pose> <output_file>"
        )
        sys.exit(1)

    # the poses are passes as JSON strings, so they need to be converted first
    initial_pose = move_cube.Pose.from_json(initial_pose_json)
    goal_pose = move_cube.Pose.from_json(goal_pose_json)

    # create a FixedInitializer with the given values
    initializer = cube_env.FixedInitializer(
        difficulty, initial_pose, goal_pose
    )

    # TODO: Replace with your environment if you used a custom one.
    if difficulty == 1:

        # we create the same env as we used for training in
        # train_pushing_ppo.py, such that action and observation space remain
        # coherent with the policy. however, unlike during  training, we set the
        # initialization using the initializer, since this is what's expected
        # during evaluation. if you do not use the initializer, or modify the
        # standard CubeEnv in any way which will affect the simulation (i.e.
        # affect the state action trajectories), the action trajectories you
        # compute will not make sense.
        env = TrainingEnv(initializer=initializer,
                                        frameskip=3,
                                        visualization=False)
        env = FlatObservationWrapper(env)

        # we load the trained policy
        policy_path = "./A2C_push/model_780000_steps.zip"
        policy = A2CPolicy(policy_path)
    elif difficulty == 2:

        # we create the same env as we used for training in
        # train_pushing_ppo.py, such that action and observation space remain
        # coherent with the policy. however, unlike during  training, we set the
        # initialization using the initializer, since this is what's expected
        # during evaluation. if you do not use the initializer, or modify the
        # standard CubeEnv in any way which will affect the simulation (i.e.
        # affect the state action trajectories), the action trajectories you
        # compute will not make sense.
        env = TrainingEnv(initializer=initializer,
                                        frameskip=3,
                                        visualization=False)
        env = FlatObservationWrapper(env)

        # we load the trained policy
        policy_path = "./A2C_lift/model_780000_steps.zip"
        policy = A2CPolicy(policy_path)

    elif difficulty == 3:

        env = TrainingEnv(initializer=initializer,
                          frameskip=3,
                          visualization=False, action_type=cube_env.ActionType.TORQUE)
        env = FlatObservationWrapper(env)
        # TODO: Replace this with your model
        policy = RandomPolicy(env.action_space)
    elif difficulty == 4:
        # TODO: Replace this with your model
        # Note: You may also use a different policy for each difficulty level (difficulty)
        # env = gym.make(
        #     "rrc_simulation.gym_wrapper:real_robot_challenge_phase_1-v1",
        #     initializer=initializer,
        #     action_type=cube_env.ActionType.POSITION,
        #     visualization=False,
        # )
        env = TrainingEnv(initializer=initializer,
                          frameskip=3,
                          visualization=False, action_type=cube_env.ActionType.TORQUE_AND_POSITION)
        env = FlatObservationWrapper(env)

        policy = RandomPolicy(env.action_space)


    # Execute one episode.  Make sure that the number of simulation steps
    # matches with the episode length of the task.  When using the default Gym
    # environment, this is the case when looping until is_done == True.  Make
    # sure to adjust this in case your custom environment behaves differently!
    is_done = False
    observation = env.reset()
    accumulated_reward = 0
    while not is_done:
        action = policy.predict(observation)
        observation, reward, is_done, info = env.step(action)
        accumulated_reward += reward

    print("Accumulated reward: {}".format(accumulated_reward))

    # store the log for evaluation
    env.platform.store_action_log(output_file)


if __name__ == "__main__":
    main()
