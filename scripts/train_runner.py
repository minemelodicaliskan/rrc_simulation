#!/usr/bin/env python3
from stable_baselines.common.vec_env import SubprocVecEnv
from stable_baselines.common.policies import MlpPolicy
from stable_baselines.common import set_global_seeds
from stable_baselines.common.callbacks import CheckpointCallback
from stable_baselines import A2C
from rrc_simulation.gym_wrapper.envs import cube_env
from scripts.training_env import TrainingEnv
from scripts.training_env import FlatObservationWrapper

import argparse
import os
import gym
import numpy as np

set_global_seeds(0)

class Runner:
    def __init__(self, difficulty):
        self.total_time_steps = 800000
        self.validate_every_timesteps = 20000
        if difficulty == 1:
            self.model_path = os.path.join("A2C_push", "training_checkpoints")
        elif difficulty == 2:
            os.path.join("A2C_lift", "training_checkpoints")
        elif difficulty == 3:
            os.path.join("A2C_lift_move", "training_checkpoints")
        else:
            os.path.join("A2C_lift_move_rotate", "training_checkpoints")
        self.num_of_active_envs = 2
        self.policy_kwargs = dict(layers=[256, 256])
        self.difficulty = difficulty
        self.train_configs = {
            "gamma": 0.99,
            "n_steps": int(1200 / 20),
            "ent_coef": 0.01,
            "learning_rate": 0.00025,
            "vf_coef": 0.5,
            "max_grad_norm": 0.5
        }


    @staticmethod
    def get_multi_process_env(num_of_envs, action_type):
        def _make_env(rank):
            def _init():
                env = TrainingEnv(frameskip=3, visualization=False, action_type=action_type)
                env.seed(seed=rank)
                env.action_space.seed(seed=rank)
                env = FlatObservationWrapper(env)
                return env

            return _init

        return SubprocVecEnv([_make_env(rank=i) for i in range(num_of_envs)])

    def train(self):
        if not os.path.exists(self.model_path):
            os.makedirs(self.model_path)
        if self.difficulty == 1:
            action_type = cube_env.ActionType.POSITION
        elif self.difficulty == 2:
            action_type = cube_env.ActionType.POSITION
        elif self.difficulty == 3:
            action_type = cube_env.ActionType.TORQUE
        else:
            action_type = cube_env.ActionType.TORQUE_AND_POSITION
        env = Runner.get_multi_process_env(self.num_of_active_envs, action_type)

        model = A2C(MlpPolicy, env, _init_setup_model=True, policy_kwargs=self.policy_kwargs,
                    **self.train_configs, verbose=1,
                    tensorboard_log=self.model_path)
        ckpt_frequency = int(self.validate_every_timesteps / self.num_of_active_envs)
        checkpoint_callback = CheckpointCallback(
            save_freq=ckpt_frequency, save_path=self.model_path, name_prefix="model"
        )

        model.learn(int(self.total_time_steps), callback=checkpoint_callback)
        env.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    #parser.add_argument("--output_path", required=True, help="output path")
    #args = vars(parser.parse_args())
    #output_path = str(args["output_path"])

    # TODO: simultaneous finger_action for task > 1
    runner = Runner(difficulty=1)
    runner.train()






